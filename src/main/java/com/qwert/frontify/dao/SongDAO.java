package com.qwert.frontify.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.qwert.frontify.exception.SongException;
import com.qwert.frontify.model.Song;

@Component
public class SongDAO {

	private List<Song> songs = new ArrayList<>();

	public SongDAO() {
		for (int i = 1; i <= 50; i++) {
			Song song = new Song();
			song.setTitle("Una musika");
			song.setDuration("9:33");
			song.setCover("picture.jpg");
//			song.getAlbum().setTitle("Something something");
//			song.getStyle().setColor("#666777");
			save(song);
		}
	}

	public void save(Song song) {
		songs.add(song);
	}
	
	public Song findByUUID(UUID uuid) throws SongException{

		// ler a lista 100
		for(Song song : songs) {
			// comparar song com uuid
			if(song.getUuid().equals(uuid))
				// retornar a musica
				return song;
		}
		
	// exception
		throw new SongException("The song ["+ uuid +"] doesn't found.");
	}	

	public List<Song> findAll() {
		return songs;
	}
}
