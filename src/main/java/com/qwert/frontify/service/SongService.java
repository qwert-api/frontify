package com.qwert.frontify.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qwert.frontify.dto.SongDTO;
import com.qwert.frontify.exception.SongException;
import com.qwert.frontify.model.Album;
import com.qwert.frontify.model.Song;
import com.qwert.frontify.repository.AlbumRepository;
import com.qwert.frontify.repository.SongRepository;

@Service
public class SongService {

	@Autowired
	private SongRepository songRepository;
	
	@Autowired
	private AlbumRepository albumRepository;

	// TODO - Refactor code duplicated // findById/update/delete
	public SongDTO save(SongDTO songDTO) {
		
		Song song = convertToSong(songDTO);
		
		Optional<Album> albumOpt = albumRepository.find1(songDTO.getAlbumTitle());
		Album album;

		if(albumOpt.isPresent()) {
			album = albumOpt.get();
		} else {
			album = new Album();
			album.setTitle(songDTO.getAlbumTitle());
		}
		
		song.setAlbum(albumRepository.save(album));			
		song.getAlbum().addSong(song);
		songRepository.save(song);

		return songDTO;
	}

	public SongDTO update(SongDTO songDTO) throws SongException {
		
		Optional<Song> song = songRepository.findById(songDTO.getId());
		if(song.isPresent()) {
			Song songFromDb = song.get();
			if(!songFromDb.getDuration().equals(songDTO.getDuration()))
				songFromDb.setDuration(songDTO.getDuration());
			if(!songFromDb.getCover().equals(songDTO.getCover()))
				songFromDb.setCover(songDTO.getCover());
			if(!songFromDb.getTitle().equals(songDTO.getTitle()))
				songFromDb.setTitle(songDTO.getTitle());
			if(!songFromDb.getAlbum().getTitle().equals(songDTO.getAlbumTitle()))
				songFromDb.getAlbum().setTitle(songDTO.getAlbumTitle());

			songFromDb.setUpdatedAt(Calendar.getInstance().getTime());
			songFromDb.getAlbum().setUpdatedAt(Calendar.getInstance().getTime());

			songRepository.save(songFromDb);
		} else {
			throw new SongException("The song ["+songDTO.getId()+"] doesn't exist."); // HttpStatus.NotFound
		}

		return songDTO;
	}
	
	public SongDTO findById(Long id) throws SongException {
		Optional<Song> song = songRepository.findById(id);
		if(song.isPresent())
			return convertToSongDTO(song.get());
		else
			throw new SongException("The song ["+id+"] doesn't exist."); // HttpStatus.NotFound
	}

	public List<SongDTO> findAll(){
		List<Song> songs = songRepository.findAll();
		List<SongDTO> songsDTO = new ArrayList<>();
		for(Song song : songs)
			songsDTO.add(convertToSongDTO(song));

		return songsDTO;
	}
	
	public Long delete(Long id) throws SongException {
		
		// TODO - verify if the songById already exist.
		// - if exist then delete
		// - if not exist then throw SongException("kfhaskdfhsafkjashkfsa") - HttpStatus.NotFound
		Optional<Song> song = songRepository.findById(id);
		if(song.isPresent()) {			
			songRepository.deleteById(id);
		} else {
			throw new SongException("The song ["+id+"] doesn't exist."); // HttpStatus.NotFound
		}
		return id;
	}
	
	private Song convertToSong(SongDTO songDTO) {
		
		Song song = new Song();
		song.setId(songDTO.getId());
		song.setUuid(songDTO.getUuid());
		song.setTitle(songDTO.getTitle());
		song.setCover(songDTO.getCover());
		song.setDuration(songDTO.getDuration());

//		song.getAlbum().setTitle(songDTO.getAlbumTitle());
//		song.getStyle().setColor(songDTO.getColor());

		return song;
	}
	
	private SongDTO convertToSongDTO(Song song) {

		SongDTO songDTO = new SongDTO();
		songDTO.setId(song.getId());
		songDTO.setUuid(song.getUuid());
		songDTO.setTitle(song.getTitle());
		songDTO.setCover(song.getCover());
		songDTO.setDuration(song.getDuration());

		songDTO.setAlbumTitle(song.getAlbum().getTitle());
//		songDTO.setColor(song.getStyle().getColor());

		return songDTO;
	}
	
	
}
