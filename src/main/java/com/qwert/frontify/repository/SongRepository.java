package com.qwert.frontify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qwert.frontify.model.Song;

@Repository
public interface SongRepository extends JpaRepository<Song, Long>{
		
// where x.uuid = ?1 or x.duration = ?2
//	public Optional<Song> findByUuid(UUID uuid);
//	public List<Song> findByUuid(UUID uuid);
//	public Song findByUuidAndDuration(UUID uuid, String duration);
//	public Song findByCreatedAtBetween(Date begin, Date end);
	
}
