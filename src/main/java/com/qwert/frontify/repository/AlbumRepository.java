package com.qwert.frontify.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.qwert.frontify.model.Album;

public interface AlbumRepository extends JpaRepository<Album, Long>{

	// Jpa Data JPQL : class / attributes <<<<<
	// Jpa Data Native Query : table / column
	// @Query(value = "select * from TB_ALBUM where title = :title", nativeQuery = true)
	@Query("select u from Album u where u.title = :title")
	public Optional<Album> find1(String title);
	
}
