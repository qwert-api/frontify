package com.qwert.frontify.model;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "TB_SONG")
public class Song {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	@Column(name="UUID")
	private UUID uuid;
	@Column(name="TITLE")
	private String title;
	@Column(name="DURATION")
	private String duration;
	@Column(name="COVER")
	private String cover;
	@Column(name="HASH")
	private String hash;
	@Column(name="CREATED_AT")
	private Date createdAt;
	@Column(name="UPDATED_AT")
	private Date updatedAt;

	@ManyToOne
	@JoinColumn(name = "album_id")
	private Album album;

//	private Style style;

	public Song() {
		uuid = UUID.randomUUID();
		createdAt = Calendar.getInstance().getTime();
		updatedAt = Calendar.getInstance().getTime();
		album = new Album();
//		style = new Style();
	}

//	public Style getStyle() {
//		return style;
//	}

//	public void setStyle(Style style) {
//		this.style = style;
//	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public UUID getUuid() {
		return uuid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
