package com.qwert.frontify.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.qwert.frontify.dto.SongDTO;
import com.qwert.frontify.exception.SongException;
import com.qwert.frontify.service.SongService;

@RestController
@RequestMapping("song")
public class SongController {
	
	@Autowired
	private SongService songService;

	@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.DELETE,RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST})
	@GetMapping
	public List<SongDTO> findAll(){		
		return songService.findAll();
	}

	@GetMapping("{id}")
	@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.DELETE,RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST})
	public ResponseEntity<?> findById(@PathVariable Long id) {
		try {
			SongDTO songDTO = songService.findById(id);
			return new ResponseEntity<SongDTO>(songDTO, HttpStatus.OK);
		} catch(SongException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.DELETE,RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST})
	@PostMapping
	public String save(@RequestBody SongDTO songDTO) {
		songService.save(songDTO);
		return "アイテムを保存しました。";
	}

	@PatchMapping
	public ResponseEntity<?> update(@RequestBody SongDTO songDTO) {
		try {
			songService.update(songDTO);
			return new ResponseEntity<SongDTO>(songDTO, HttpStatus.OK);
		} catch(SongException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			songService.delete(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch(SongException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.DELETE,RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST})
	@GetMapping("hello")
		public String hello() {
		return "Hello, world!";
	}
}
