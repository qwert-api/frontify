package com.qwert.frontify.dto;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SongDTO {

	private UUID uuid;

	private Long id;
	@JsonProperty("album-title")
	private String albumTitle;
	@JsonProperty("artist-name")
	private String artistName;
	@JsonProperty("song-title")
	private String title;
	private String duration;
	private String cover;
	private String color;
	
	public SongDTO() {
		uuid = UUID.randomUUID();
		id = 0L;
		albumTitle = "";
		artistName = "";
		title = "";
		duration = "";
		cover = "";
		color = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
